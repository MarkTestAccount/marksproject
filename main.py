import sys

def check_odd_even():
    number = float(sys.argv[1])
    decimal_number = int(number)
    if number % 2 == 0:
        print ('The number passed in was', decimal_number, 'and is an Even number')
    else:
        print ('The number passed in was', decimal_number, 'and is an Odd number')

if __name__ == "__main__":

    check_odd_even()
