import requests
import sys
 
api_key = '79c13661'
base_url = 'http://www.omdbapi.com/'

# Function to fetch movie details by title
def get_movie_details():

    if len(sys.argv) < 2:
        print ("Error please provide a film title as a command line argument")
        return None

    title = (sys.argv[1])
    params = {'apikey': api_key, 't': title}
    response = requests.get(base_url, params=params)

    if response.status_code == 200:
        movie_data = response.json()
        if movie_data.get('Response') == 'True':
            return movie_data
        else:
            print(f"Error: {movie_data.get('Error')}")
    else:
        print(f"Error: {response.status_code}, {response.text}")

movie_details = get_movie_details()

if movie_details:
    print(f"Title: {movie_details['Title']}")
    print(f"Year: {movie_details['Year']}")
    print(f"Plot: {movie_details['Plot']}")
    # Add more details as needed


if __name__ == "__mimdb__":

    get_movie_details()
