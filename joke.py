import requests

def get_joke():
    url = 'https://v2.jokeapi.dev/joke/Any' # You can customize the category or type of joke as needed

    try:
        response = requests.get(url)
        response.raise_for_status() # Raise an HTTPError for bad responses

        joke_data = response.json()

        if joke_data['type'] == 'twopart':
            return f"{joke_data['setup']} {joke_data['delivery']}"
        else:
            return joke_data['joke']

    except requests.exceptions.RequestException as e:
        return f"Error: {e}"

if __name__ == "__main__":
    joke = get_joke()
    print(joke)
