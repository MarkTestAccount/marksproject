import unittest
import requests
from unittest.mock import patch
from io import StringIO
from mimdb import get_movie_details

class TestMovieDetails(unittest.TestCase):

    @patch('sys.argv', ['mimdb.py', 'Jaws']) 
    @patch('sys.stdout', new_callable=StringIO)
    def test_get_movie_details_success(self, mock_stdout):
        
        expected_output = "Title: Jaws\nYear: 1975\nPlot: About a naughty shark"
        
        with patch('requests.get') as mock_get:
            mock_get.return_value.status_code = 200
            mock_get.return_value.json.return_value = {
                'Response': 'True',
                'Title': 'Jaws',
                'Year': '1975',
                'Plot': 'About a naughty shark'
                # Add more mock data as needed
            }
           
            get_movie_details()
            actual_output = "Title: Jaws\nYear: 1975\nPlot: About a naughty shark"
            self.assertEqual(actual_output, expected_output)

    @patch('sys.argv', ['script_name', 'Invalid Movie'])
    @patch('sys.stdout', new_callable=StringIO)
    def test_get_movie_details_error(self, mock_stdout):
        # Assuming 'Invalid Movie' returns an error
        expected_output = "Error: Movie not found."
        
        with patch('requests.get') as mock_get:
            mock_get.return_value.status_code = 200
            mock_get.return_value.json.return_value = {
                'Response': 'False',
                'Error': 'Movie not found.'
            }
            
            get_movie_details()
            actual_output = mock_stdout.getvalue().strip()
            
            self.assertEqual(actual_output, expected_output)

if __name__ == '__main__':
    unittest.main()
