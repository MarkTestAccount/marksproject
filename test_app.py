import unittest
from unittest.mock import patch
from io import StringIO
import sys
from main import check_odd_even

class TestPrintEvenOrOddFromInput(unittest.TestCase):
    @patch('sys.stdout', new_callable=StringIO)
    def test_print_even(self, mock_stdout):
        with patch.object(sys, 'argv', ['main.py', '2']):
            check_odd_even()
            self.assertEqual(mock_stdout.getvalue().strip(), 'The number passed in was 2 and is an Even number')

    @patch('sys.stdout', new_callable=StringIO)
    def test_print_odd(self, mock_stdout):
        with patch.object(sys, 'argv', ['main.py', '3']):
            check_odd_even()
            self.assertEqual(mock_stdout.getvalue().strip(), 'The number passed in was 3 and is an Odd number')

if __name__ == '__main__':
    unittest.main()
